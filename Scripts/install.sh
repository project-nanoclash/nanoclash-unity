#! /bin/sh

# This link changes from time to time. I haven't found a reliable hosted installer package for doing regular
# installs like this. You will probably need to grab a current link from: http://unity3d.com/get-unity/download/archive
echo 'Downloading from http://mirrors.aunali1.com/project-nanoclash/ci/unity-editor-5.3.3f1.tar.bz2: '
curl -o unity-editor-5.3.3f1.tar.bz2 http://mirrors.aunali1.com/project-nanoclash/ci/unity-editor-5.3.3f1.tar.bz2


echo 'Unpacking unity-editor-5.3.3f1.tar.bz2'

tar -xf unity-editor-5.3.3f1.tar.bz2
