#! /bin/sh

# Change this the name of your project. This will be the name of the final executables as well.
project="ci-build"

echo "Attempting to build $project for Windows"
$(pwd)/unity-editor-5.3.3f1/Editor/Unity \
  -batchmode \
  -silent-crashes \
  -logFile $(pwd)/unity.log \
  -projectPath $(pwd) \
  -buildWindowsPlayer "$(pwd)/Build/windows/$project.exe" \
  -quit

echo "Attempting to build $project for OS X"
$(pwd)/unity-editor-5.3.3f1/Editor/Unity \
  -batchmode \
  -silent-crashes \
  -logFile $(pwd)/unity.log \
  -projectPath $(pwd) \
  -buildOSXUniversalPlayer "$(pwd)/Build/osx/$project.app" \
  -quit

echo "Attempting to build $project for Linux"
$(pwd)/unity-editor-5.3.3f1/Editor/Unity \
  -batchmode \
  -silent-crashes \
  -logFile $(pwd)/unity.log \
  -projectPath $(pwd) \
  -buildLinuxUniversalPlayer "$(pwd)/Build/linux/$project" \
  -quit

echo 'Logs from build'
cat $(pwd)/unity.log
